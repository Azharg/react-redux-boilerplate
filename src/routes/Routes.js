import React, { Suspense, useEffect } from "react";
import {
  Router,
  Switch,
  useLocation,
  useHistory,
} from "react-router-dom";
import history from "./History";
import * as LazyComponent from "../utils/LazyLoaded";
import Loader from "../components/Loader/Loader";

const Routes = () => {
  const location = useLocation();
  const History = useHistory();

  return (
    <Suspense fallback={<Loader />}>
      <Router history={history}>
        <Switch>
          <LazyComponent.Home path="/" exact />
          <LazyComponent.Login path="/login" exact />
        </Switch>
      </Router>
    </Suspense>
  );
};

export default Routes;
