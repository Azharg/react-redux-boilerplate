import { combineReducers } from "redux";
import loader from "../Loader/LoaderReducer";
import Feature1 from "../Feature1/FeatureReducer";

export default combineReducers({
  loader,
  Feature1
});
