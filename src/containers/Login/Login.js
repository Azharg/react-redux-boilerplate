import React from 'react';
import History from '../../routes/History';
class Login extends React.Component {

    // this method is only to trigger route guards , remove and use your own logic
    handleLogin = () => {
        localStorage.setItem('token','token');
        History.push('/')
    }

    render(){
        return(
            <div className="container my-5">
                <h1>Login Page</h1>
                <button onClick={this.handleLogin}>Login</button>
            </div>
        )
    }
}


export default Login;